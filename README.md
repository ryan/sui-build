# Sui Reproducible Builds

Uses the [StageX] software distribution for ensuring a reproducible toolchain.

## Building

```sh
make sui
```

## Starting Sui

The Sui container can be imported by running:

```sh
env -C out/sui tar -c . | docker load
```

To make sure Sui starts properly, run:

```sh
docker run sui
```

The version should be printed.
